var Coookie = (function(){

    var setItem =  function(key, value, hour = null){
        localStorage.setItem(key, value);
        sessionStorage.setItem(key, value);
        var cookieString = key + "=" + value + ";path=/;";
        if(hour != null && typeof hour === "number"){
            cookieString += ";max-age=" + hour * 60 * 60;
        }
        document.cookie = cookieString;
    }

    var getItem = function(key){
        var val = '';
        val = localStorage.getItem(key);
        if( val !== '' ){
            return val;
        }
        val = sessionStorage.getItem(key);
        if( val !== '' ){
            return val;
        }
        var cookies = document.cookie.split('; ');
        for(var i=0,len=cookies.length;i<len;i++){
            var cookie = cookies[i].split('=');
            if(cookie[0] === key){
                return cookie[1];
            }
        }
    }

    return {
        setItem: setItem,
        getItem: getItem
    }

})();


var cookieTakeover = (function(ck){

  var key;
  var value;

  function get_value(str){
    if(str.indexOf('?') < 0){
      return str += '?' + key  + '=' + value;
    }else{
      return str += '&' + key  + '=' + value;
    }
  }

  function startProcess(time,getkey){
    setTimeout(function(){
      key = getkey;
      value = ck.getItem(getkey);
      setAncher();
    },time)
  }

  function setAncher(){
    var anckers = document.querySelectorAll('a');
    for(var i = 0;i < anckers.length; i++){
      if( !(anckers[i].href.indexOf('#') >= 0) && !(anckers[i].href.trim() == '') ){
        anckers[i].href = get_value(anckers[i].href);
      }
    }
  }

  return {
    startProcess: startProcess
  }
})(Coookie);

cookieTakeover.startProcess(2000,"_lag_id");
